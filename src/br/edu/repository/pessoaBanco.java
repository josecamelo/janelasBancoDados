package br.edu.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class pessoaBanco {

	/**
	 *  Estudem!!!
	 *	delete  
	 *	update executeUpdate //estado do banco de dados
	 *	insert 
	 *	select  executeQuery
	 * @throws Exception 
	 */
	
	public void inserir(String codigo,String nome) throws Exception {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("insert into pessoa values(?,?)");
		stmt.setInt(1, Integer.parseInt(codigo));
		stmt.setString(2, nome);
		if (nome.equals("")) {
			throw new Exception("Nome não pode ser vazio!!");
		} else {
			stmt.executeUpdate();
		}	
		stmt.close();
		conn.close();
	}
	
	public void apagar(String codigo) throws SQLException {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("delete from pessoa where codigo=?");
		stmt.setInt(1, Integer.parseInt(codigo));
		stmt.executeUpdate();
		stmt.close();
		conn.close();		
	}
	
	public void atualizar(String codigo, String nome) throws SQLException {
		Connection conn = Conexao.getConexao();
		PreparedStatement stmt = conn.prepareStatement("update pessoa set nome=? where codigo=?");
		stmt.setString(1, nome);		
		stmt.setInt(2, Integer.parseInt(codigo));
		stmt.executeUpdate();
		stmt.close();
		conn.close();
	}
	
	public List<String> listarTodos(){
		List<String> nomePessoas = new ArrayList<String>();
		try {
			Connection conn = Conexao.getConexao();
			Statement stm = conn.createStatement();
			ResultSet rs = stm.executeQuery("select * from pessoa");
			while (rs.next()) {
				nomePessoas.add(rs.getString(2));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return nomePessoas;
	}
	
	public String listar(String codigo) {
		String nomePessoa="";
		
		try {
			Connection conn = Conexao.getConexao();
			PreparedStatement stmt = conn.prepareStatement("select nome from pessoa where codigo=?");	
			stmt.setInt(1, Integer.parseInt(codigo));
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				nomePessoa = rs.getString(1);
			}
			if (nomePessoa.equals("")){
				throw new Exception("O nome do cliente está em branco!!");
			}
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println("Mensagem: "+e.getMessage());
		}
		return nomePessoa;
	}
	
}
