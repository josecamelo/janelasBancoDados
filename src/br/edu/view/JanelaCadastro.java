package br.edu.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import br.edu.repository.pessoaBanco;

public class JanelaCadastro extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6832829989494438253L;
	private JButton inserirButton;
	private JButton apagarButton;
	private JButton atualizarButton;
	private JButton selecionarButton;
	private JButton listarButton;
	
	private JLabel codigoLabel;
	private JTextField codigoTextField;
	
	private JLabel nomeLabel;
	private JTextField nomeTextField;
	
	private JPanel cadastroPanel;
	private JPanel botoesPanel;
	
	private Container container;
	
	//Conexao com o banco de dados;
	private pessoaBanco pB = new pessoaBanco();
	
	public JanelaCadastro() {
		super("Exemplo de cadastro!!");
		
		codigoLabel = new JLabel("Codigo");
		codigoTextField = new JTextField(10);
		
		nomeLabel = new JLabel("Nome");
		nomeTextField = new JTextField(30);
		
		cadastroPanel = new JPanel(new GridLayout(4,1));
		cadastroPanel.add(codigoLabel);
		cadastroPanel.add(codigoTextField);
		cadastroPanel.add(nomeLabel);
		cadastroPanel.add(nomeTextField);
		
		
		inserirButton = new JButton("Inserir");
		inserirButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				try {
					pB.inserir(codigoTextField.getText(), nomeTextField.getText());
					JOptionPane.showMessageDialog(null,"Registro inserido com sucesso!!");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					JOptionPane.showMessageDialog(null, "Você errou!!"+e.getMessage());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
			
		});
		
		selecionarButton = new JButton("Selecionar");
		selecionarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				nomeTextField.setText("");
				nomeTextField.setText(pB.listar(codigoTextField.getText()));			
			}
			
		});		
		apagarButton = new JButton("Apagar");
		apagarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				try {
					pB.apagar(codigoTextField.getText());
					codigoTextField.setText("");
					nomeTextField.setText("");
					JOptionPane.showMessageDialog(null,"Registro excluído com sucesso!!");
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Você errou!!"+e.getMessage());
				}
			}
			
		});		
		atualizarButton = new JButton("Atualizar");
		atualizarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				try {
					pB.atualizar(codigoTextField.getText(), nomeTextField.getText());
					JOptionPane.showMessageDialog(null,"Registro atualizado com sucesso!!");					
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Você errou!!"+e.getMessage());
				}
				
			}
			
		});		
		
		listarButton = new JButton("Listagem");
		listarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JanelaListagem jl=new JanelaListagem();
			}
			
		});
		
		botoesPanel = new JPanel(new GridLayout(1,5));
		botoesPanel.setPreferredSize(new Dimension(100,50));
		botoesPanel.add(inserirButton);
		botoesPanel.add(selecionarButton);
		botoesPanel.add(apagarButton);
		botoesPanel.add(atualizarButton);
		botoesPanel.add(listarButton);
		
		container = getContentPane();
		container.setLayout(new BorderLayout());
		container.add(cadastroPanel, BorderLayout.CENTER);
		container.add(botoesPanel, BorderLayout.SOUTH);
		
		setSize(500,250);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
	}
	
	public static void main(String[] args) {
		JanelaCadastro jc = new JanelaCadastro();
	}
	
}
